/// Students

// create service accounts

for i in {1..15}; 
do

k create ns t$i;
k create sa t$i -n t$i;
k create role t$i --resource="*.*" --verb="*" -n t$i;
k create rolebinding t$i --role t$i --serviceaccount t$i\:t$i -n t$i;

k apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: t$i-secret
  namespace: t$i
  annotations:
    kubernetes.io/service-account.name: t$i
type: kubernetes.io/service-account-token
EOF

done

// create service account kube configs

for i in {1..15}; 
do

./create_kube_config.sh t$i t$i;

done

// create ssh keypairs 

for i in {1..15}; do
ssh-keygen -t rsa -m pem -q -N "" -f sshkey/t${i}_rsa;
done

// create ubuntu accounts

gcloud compute ssh kd-ubuntu-set --zone asia-southeast1-a;
for i in {1..15}; 
do
sudo useradd -s /bin/bash t$i -m -d /home/t$i;
sudo mkdir -p /home/t$i/.ssh;
sudo mkdir -p /home/t$i/.kube;
sudo chown -R t$i:t$i /home/t$i/;
done

// delete ubuntu accounts
for i in {1..12}; 
do
sudo userdel t$i
sudo rm -rf /home/t$i;
done

// copy kube configs and ssh key pairs

for ia in {1..15}; 
do
gcloud compute scp sshkey/t${ia}_rsa.pub root@kd-ubuntu-set:/home/t$ia/.ssh/authorized_keys;
gcloud compute scp kubeconfig/t$ia root@kd-ubuntu-set:/home/t$ia/.kube/config;
done

// grant user permissions to the ubuntu accounts

gcloud compute ssh kd-ubuntu-set;
for i in {1..15}; 
do
sudo chown -R t$i:t$i /home/t$i/;
done

/// Admin

// create service account

k create ns staffkube;
k create sa staffkube -n staffkube;
k create clusterrolebinding staffkube --clusterrole cluster-admin --serviceaccount=staffkube:staffkube;

// create service account kube config

k apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: staffkube-secret
  namespace: staffkube
  annotations:
    kubernetes.io/service-account.name: staffkube
type: kubernetes.io/service-account-token
EOF

./create_kube_config.sh staffkube staffkube;

// create ssh keypairs 

ssh-keygen -t rsa -m pem -q -N "" -f sshkey/staffkube_rsa;

// create ubuntu account

gcloud compute ssh kd-ubuntu-set;
sudo useradd staffkube -s /bin/bash -m -d /home/staffkube;
sudo mkdir -p /home/staffkube/.ssh;
sudo mkdir -p /home/staffkube/.kube;
sudo chown -R staffkube:staffkube /home/staffkube/;

// delete ubuntu account
sudo userdel staffkube
sudo rm -rf /home/staffkube

// copy kube configs and ssh key pairs

gcloud compute scp sshkey/staffkube_rsa.pub root@kd-ubuntu-set:/home/staffkube/.ssh/authorized_keys
gcloud compute scp kubeconfig/staffkube root@kd-ubuntu-set:/home/staffkube/.kube/config

// grant user permissions to the ubuntu account

gcloud compute ssh kd-ubuntu-set;
sudo chown -R staffkube:staffkube /home/staffkube/;